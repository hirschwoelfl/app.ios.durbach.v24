//
//  HWKommunalFrame.h
//  HWKommunalFrame
//
//  Created by Shehryar Khan on 29.10.19.
//  Copyright © 2019 Shehryar Khan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>


//! Project version number for HWKommunalFrame.
FOUNDATION_EXPORT double HWKommunalFrameVersionNumber;

//! Project version string for HWKommunalFrame.
FOUNDATION_EXPORT const unsigned char HWKommunalFrameVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HWKommunalFrame/PublicHeader.h>


