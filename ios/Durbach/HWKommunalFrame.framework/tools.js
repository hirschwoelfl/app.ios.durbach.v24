function imageSourceFromPoint(x, y) {
    var element = document.elementFromPoint(x, y);
    if (element.tagName == 'img' && element.src) {
        return element.src;
    }
    return null;
}


function getImages(){
    var objs = document.getElementsByTagName('img');
    var imgScr = '';
    for(var i=0;i<objs.length;i++){
        imgScr = imgScr + objs[i].src + '+';
        
    };
    return imgScr;
    
}


function getLongImages(){
    var objs = document.getElementsByTagName('a');
    var imgScr = '';
    for(var i=0;i<objs.length;i++){
        imgScr = imgScr + objs[i].href + '+';
        
    };
    return imgScr;
    
}


function getImagesTitleCaption(){
    var objs = document.getElementsByTagName('figcaption');
    var imgScr = '';
    for(var i=0;i<objs.length;i++){
        imgScr = imgScr + objs[i].innerHTML + '+';
        
    };
    return imgScr;
    
}

function getImagesTitle(){
    var objs = document.getElementsByTagName('img');
    var title = '';
    for(var i=0;i<objs.length;i++){
        var titleTemp = objs[i].title;
        
        if ( titleTemp === '' ) {
            var figure = objs[i].parentNode.parentNode;
            
            if ( typeof figure !== 'undefined' && figure ) {
                var tagName = figure.tagName.toUpperCase();
                
                if ( tagName === 'FIGURE' ) {
                    
                    var figcaption = figure.children[1];
                    
                    if ( typeof figcaption !== 'undefined' && figcaption ) {
                        var tagNameCaption = figcaption.tagName.toUpperCase();
                        
                        if ( tagNameCaption === 'FIGCAPTION' ) {
                            
                            titleTemp = figcaption.innerHTML;
                            
                        }
                        
                        
                    }
                    
                }
                
                
            }
        }
        
        title = title + titleTemp + '+';
    };
    return title;
    
}


