//
//  AppDelegate.swift
//  Durbach
//
//  Created by Christian Drost on 21.07.23.
//

import UIKit
import HWKommunalFrame
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var lastActiveTimestamp:Double = 0.0
    
    var defaults = UserDefaults.standard
    let notificationDelegate = CustomNotificationDelegate()
    var lastActive:String = "lastActive"
    
    var fontSizeMultiplier : Int {
        get {
            switch UIApplication.shared.preferredContentSizeCategory {
            case UIContentSizeCategory.accessibilityExtraExtraExtraLarge: return 23
            case UIContentSizeCategory.accessibilityExtraExtraLarge: return 22
            case UIContentSizeCategory.accessibilityExtraLarge: return 21
            case UIContentSizeCategory.accessibilityLarge: return 20
            case UIContentSizeCategory.accessibilityMedium: return 19
            case UIContentSizeCategory.extraExtraExtraLarge: return 19
            case UIContentSizeCategory.extraExtraLarge: return 18
            case UIContentSizeCategory.extraLarge: return 17
            case UIContentSizeCategory.large: return 16
            case UIContentSizeCategory.medium: return 15
            case UIContentSizeCategory.small: return 14
            case UIContentSizeCategory.extraSmall: return 13
            default: return 15
            }
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let center = UNUserNotificationCenter.current()
        center.delegate = notificationDelegate
        
        DefaultConfig.fontSizeCategory = fontSizeMultiplier
        print("checkFont-size ", DefaultConfig.fontSizeCategory)
        
        let _ = MainConfig.shared
        let _ = DataFetch.sharedInstance
        UIFont.loadMyFonts
        
        Thread.sleep(forTimeInterval: 1.0)
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        if #available(iOS 13.0, *) {
            window?.backgroundColor = UIColor.systemBackground
        } else {
            window?.backgroundColor = UIColor.white
        }
        self.registerForPushNotifications()
        
        print("fetch user",UserDefaults.isLogoutFetch())
        DefaultConfig.defaultTintColor = UIColor(DefaultConfig.defaultProjectTintColor)

        self.openViewControllerFromRemoteNotification(launchOptions)
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        //        pushNotificationAuthCheck()
        print("applicationDidBecomeActiveZUvfverv")
        if #available(iOS 13.0, *) {
            
        } else {
            refreshUIColor()
        }
    }
    
    @objc
    func refreshUIColor() {
        //           let tintColor = MainConfig().defaultTintColor
        DefaultConfig.defaultTintColor = UIColor(DefaultConfig.defaultProjectTintColor)
        let tintColor = DefaultConfig.defaultTintColor
        window?.tintColor = tintColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : tintColor]
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold) as Any]
        UINavigationBar.appearance().isTranslucent = false
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().tintColor = DefaultConfig.defaultTintColor
        
    }
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("applicationDidEnterBackground ")
    }
    func applicationWillTerminate(_ application: UIApplication) {
        print("applicationWillTerminate ")
        
    }
    
}


//***********************************************************************************************************//
//             Below section is only for push notification delegate with Custom UI
//***********************************************************************************************************//

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func registerForPushNotifications() {
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = notificationDelegate
            center.requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                print("Permission granted: \(granted)")
                print("delegateFirstLaunch \(AppConfig.isFirstLaunch)")
                if AppConfig.isFirstLaunch {
                    if granted {
                        let notifi = NotificationsFirstLaunchToServer.sharedInstance
                        notifi.callInitManual(dashboardItemsRemove: true)
                        
                    }else{
                        
                    }
                }else{
                    
                }
                AppConfig.pushNotificationEnableFlag = granted
                // 1. Check if permission granted
                guard granted else { return }
                self.getNotificationSettings()
                // 2. Attempt registration for remote notifications on the main thread
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
            let openAction = UNNotificationAction(identifier: "OpenNotification", title: NSLocalizedString("Htetinglo ", comment: ""), options: UNNotificationActionOptions.foreground)
            let deafultCategory = UNNotificationCategory(identifier: "CustomPush", actions: [openAction], intentIdentifiers: [], options: [])
            center.setNotificationCategories(Set([deafultCategory]))
            
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        
        defaults.set("\(token)", forKey: "token")
    }
    
    func application( _ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("didReceiveRemoteNotification")
        guard let aps = userInfo["aps"] as? [String: AnyObject] else {
            completionHandler(.failed)
            return
        }
        
        completionHandler(.newData)
        print("didReceiveRemoteNotification",aps)
        
        
        //        NewsItem.makeNewsItem(aps)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received: \(data)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
    }
    
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
        }
    }
    
    func openViewControllerFromRemoteNotification(_ launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        // notification redirect to ViewController
        
        // Check if launched from notification
        let notificationOption = launchOptions?[.remoteNotification]
        
        // 1
        if let notification = notificationOption as? [String: AnyObject],
           let aps = notification["aps"] as? [String: AnyObject] {
            print(aps)
        }
    }
    
}

extension UIApplication {
    static var appVersion: String? {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }
}
