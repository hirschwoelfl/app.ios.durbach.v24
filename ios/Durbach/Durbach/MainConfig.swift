//
//  MainConfig.swift
//  Schwaebisch Hall
//
//  Created by Shehryar Khan on 28.07.21.
//

import Foundation
import UIKit
import HWKommunalFrame
import MapKit

open class MainConfig {
    
    public static let shared = MainConfig()
    
    private init() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        DefaultConfig.defaultProjectTintColor = "#374A53"
        DefaultConfig.baseUrl = "https://www.gemeinde-durbach.de/index.php?id=248&baseColor=374A53&baseFontSize=\(DefaultConfig.fontSizeCategory)"
        
        DefaultConfig.homepageUrl = URL(string: "https://www.gemeinde-durbach.de/?appv3mode=yes")
        DefaultConfig.maengelmeldungServerUrl = "https://www.gemeinde-durbach.de/index.php?id=248&action=sendMaengelmeldung"
        
                
        DefaultConfig.bundleId = Bundle.main.bundleIdentifier ?? ""
        DefaultConfig.version = UIApplication.appVersion ?? ""
                
        DefaultConfig.categoryLabelCornerRadius = CGFloat(8.0)
        DefaultConfig.dayLabelFontColor = UIColor("#272727")
        
        DefaultConfig.settingsIconTintColor = "#fffff"
       
        DefaultConfig.defaultLatitude = 48.4945103
        DefaultConfig.defaultLongitude = 8.029079
       
        DefaultConfig.latitudinalMeters = 10000
        DefaultConfig.longitudinalMeters = 10000
        
        DefaultConfig.wetterModuleDateHidden = false
        DefaultConfig.wetterModuleHidden = true
        DefaultConfig.newsCellDateAndIconHidden = false
        
        DefaultConfig.categoryLabelTexColor = UIColor("#FFFFFF")
        
        DefaultConfig.fontSizeCategory = 15
        
        DefaultConfig.calendarIconImageName = "calenderIcon"
        
        DefaultConfig.dateDayLabelTextColor = "#FFFFFF"
        DefaultConfig.weatherIconTextColor = "#FFFFFF"
        DefaultConfig.temperatureLabelTextColor = "#FFFFFF"
        DefaultConfig.weatherTintColor = "#FFFFFF"
        
    }
    
    @objc
    func updateBecomeActive() {
        
        var fontSizeMultiplier : Int {
            get {
                if UIDevice.isPad {
                    switch UIApplication.shared.preferredContentSizeCategory {
                    case UIContentSizeCategory.accessibilityExtraExtraExtraLarge: return 23
                    case UIContentSizeCategory.accessibilityExtraExtraLarge: return 22
                    case UIContentSizeCategory.accessibilityExtraLarge: return 21
                    case UIContentSizeCategory.accessibilityLarge: return 20
                    case UIContentSizeCategory.accessibilityMedium: return 19
                    case UIContentSizeCategory.extraExtraExtraLarge: return 19
                    case UIContentSizeCategory.extraExtraLarge: return 18
                    case UIContentSizeCategory.extraLarge: return 17
                    case UIContentSizeCategory.large: return 16
                    case UIContentSizeCategory.medium: return 15
                    case UIContentSizeCategory.small: return 14
                    case UIContentSizeCategory.extraSmall: return 13
                    default: return 15
                    }
                }else{
                    
                    switch UIApplication.shared.preferredContentSizeCategory {
                    case UIContentSizeCategory.accessibilityExtraExtraExtraLarge: return 23
                    case UIContentSizeCategory.accessibilityExtraExtraLarge: return 22
                    case UIContentSizeCategory.accessibilityExtraLarge: return 21
                    case UIContentSizeCategory.accessibilityLarge: return 20
                    case UIContentSizeCategory.accessibilityMedium: return 19
                    case UIContentSizeCategory.extraExtraExtraLarge: return 19
                    case UIContentSizeCategory.extraExtraLarge: return 18
                    case UIContentSizeCategory.extraLarge: return 17
                    case UIContentSizeCategory.large: return 16
                    case UIContentSizeCategory.medium: return 15
                    case UIContentSizeCategory.small: return 14
                    case UIContentSizeCategory.extraSmall: return 13
                    default: return 15
                    }
                }
                
            }
        }
        print("rtupdateBecomeActive=charsi\(DefaultConfig.fontSizeCategory)")
        DefaultConfig.fontSizeCategory = fontSizeMultiplier
        print("rtupdateBecomeActive=ciAgain  \(DefaultConfig.fontSizeCategory)")
        
        DefaultConfig.baseUrl = "https://www.gemeinde-durbach.de/index.php?id=248&baseColor=374A53&baseFontSize=\(fontSizeMultiplier)"
        print("rtupdateBecomOriginal   \(DefaultConfig.baseUrl)")
        
        // FOR PUSH NOTIFICATIONS
        
        //        DefaultConfig.appName = "Vellberg"
        //        DefaultConfig.appIndexID = 483
        //        DefaultConfig.pushInternalKey = "hw.vellberg"
        
        DefaultConfig.appName = "Gemeinde Durbach"
        DefaultConfig.appIndexID = 1238
        DefaultConfig.pushInternalKey = "hw.durbach"
        
        DefaultConfig.pinWithUserLocationShowFlag = true
    }
}
