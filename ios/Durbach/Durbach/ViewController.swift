//
//  ViewController.swift
//  Durbach
//
//  Created by Christian Drost on 21.07.23.
//

import UIKit
import HWKommunalFrame

class ViewController: UIViewController {
    
    var phoneDashboardStoryboardName:String = "PhoneDashboard"
    var padDashboardStoryboardName:String = "PadDashboard"

    var phoneDashboardControllerIdentifier:String = "DashboardViewController"
    var padDashboardControllerIdentifier:String = "PadDashboardViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.goToMainDashboard()
        
        print("I am check Version",  DefaultConfig.bundleId, DefaultConfig.version)
        
        print("ViewControllerBecomOriginal   \(DefaultConfig.baseUrl)")
        
        print("firstLauch \(AppConfig.isFirstLaunch)")
        
    }
    
}


extension ViewController {
    
    fileprivate func goToMainDashboard() {
        if UIDevice.isPhone {
            print("goToPhoneDashboardAnonymously")

            let storyBoard : UIStoryboard = UIStoryboard(name: self.phoneDashboardStoryboardName, bundle: Bundle(for: DashboardViewController.self))
            
            if let dashboardVC = storyBoard.instantiateViewController(withIdentifier: self.phoneDashboardControllerIdentifier) as? DashboardViewController {
                dashboardVC.modalPresentationStyle = .fullScreen
                
                if #available(iOS 13.0, *) {
                    dashboardVC.isModalInPresentation = true
                } else {
                    // Fallback on earlier versions
                }
                
                let dashboardNavigationContoller = UINavigationController(rootViewController: dashboardVC)
                dashboardNavigationContoller.modalPresentationStyle = .fullScreen
                
                self.present(dashboardNavigationContoller, animated: false, completion: nil)
            }
            
        } else if UIDevice.isPad {
            print("goToPadDashboardAnonymously")

            let storyBoard : UIStoryboard = UIStoryboard(name: self.padDashboardStoryboardName, bundle: Bundle(for: PadDashboardViewController.self))
            
            if let dashboardVC = storyBoard.instantiateViewController(withIdentifier: self.padDashboardControllerIdentifier) as? PadDashboardViewController {
                dashboardVC.modalPresentationStyle = .fullScreen
                
                if #available(iOS 13.0, *) {
                    dashboardVC.isModalInPresentation = true
                } else {
                    // Fallback on earlier versions
                }
                
                let dashboardNavigationContoller = UINavigationController(rootViewController: dashboardVC)
                dashboardNavigationContoller.modalPresentationStyle = .fullScreen
                
                self.present(dashboardNavigationContoller, animated: false, completion: nil)
            }
        }
  
    }
}
